package py.com.personal.demoregula.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;

public class FontUtils {
    private static final String PLATFORM = "Platform";
    private static final HashMap<String, HashMap<Integer, Typeface>> fuentes = new HashMap<>();

    /**
     * Setea la fuente (tipografia) de un View (extiende TextView)
     *
     * @param context Contexto (Solo usado la primera vez para crear la clase
     *                Typeface desde assets //ver
     * @param view    Vista cuya fuente sera cambiada
     * @param style   Estilo a implementar (debe ser un int de
     *                Typeface.{BOLD|ITALIC|ETC}
     */
    public static <T extends TextView> void setearFuente(Context context, T view, int style) {
        HashMap<Integer, Typeface> hash = fuentes.get(PLATFORM);
        if (hash == null) {
            hash = new HashMap<>();
            fuentes.put(PLATFORM, hash);
        }
        Typeface font = fuentes.get(PLATFORM).get(style);
        if (font == null) {
            font = Typeface.createFromAsset(context.getAssets(), getTypeFaceString(style));
            hash.put(style, font);
        }
        view.setTypeface(font);
    }

    private static String getTypeFaceString(int style) {
        switch (style) {
            case Typeface.NORMAL:
            case Typeface.ITALIC:
                return "fonts/Platform-Regular.otf";
            case Typeface.BOLD:
            case Typeface.BOLD_ITALIC:
                return "fonts/Platform-Medium.otf";
        }
        return null;
    }
}
