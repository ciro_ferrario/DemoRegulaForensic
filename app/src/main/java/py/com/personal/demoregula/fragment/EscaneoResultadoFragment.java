package py.com.personal.demoregula.fragment;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.regula.documentreader.api.enums.eGraphicFieldType;
import com.regula.documentreader.api.enums.eVisualFieldType;
import com.regula.documentreader.api.results.DocumentReaderResults;
import com.regula.facesdk.results.MatchFacesResponse;
import com.regula.facesdk.results.MatchedFacesPair;
import com.regula.facesdk.structs.Image;
import com.regula.facesdk.structs.MatchFacesRequest;

import java.util.Date;

import py.com.personal.demoregula.R;
import py.com.personal.demoregula.utils.DateUtils;
import py.com.personal.demoregula.utils.FontUtils;

public class EscaneoResultadoFragment extends Fragment {
    public TextView txt_similitud;
    public EditText edtxtNroDoc, edtxtFechaNac, edtxtNombres, edtxtApellidos;
    ;
    public RadioGroup radioGroupSexo, radioGroupComparacion;
    public ImageView docImageIv, realIv;
    public LinearLayout authResults, ly_resultadoRadioGroup, ly_user_data, ly_cedula_data_title;
    public DocumentReaderResults results;
    public MatchFacesRequest request;
    public MatchFacesResponse response;
    private Button btn_again, btn_next;
    public static EscaneoResultadoFragment instance;

    public static EscaneoResultadoFragment getInstance(DocumentReaderResults results) {
        return getInstance(results, null, null);
    }

    public static EscaneoResultadoFragment getInstance(DocumentReaderResults results, MatchFacesRequest request, MatchFacesResponse response) {
        if (instance == null) {
            instance = new EscaneoResultadoFragment();
        }
        instance.results = results;
        instance.request = request;
        instance.response = response;
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_escaneo_resultado, container, false);
        setearReferencias(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        displayResults(this.results);
    }

    private void setearReferencias(View view) {
        ly_resultadoRadioGroup = view.findViewById(R.id.resultado_readiogroup);
        edtxtNroDoc = view.findViewById(R.id.txtNroDoc);
        ly_cedula_data_title = view.findViewById(R.id.linear_title_datoscedula);
        edtxtFechaNac = view.findViewById(R.id.txtFechaNac);
        radioGroupSexo = view.findViewById(R.id.radiogroup_sexo);
        radioGroupComparacion = view.findViewById(R.id.radiogroup_comparacion);
        edtxtNombres = view.findViewById(R.id.txtNombres);
        ly_user_data = view.findViewById(R.id.linear_data_user);
        edtxtApellidos = view.findViewById(R.id.txtApellidos);
        txt_similitud = view.findViewById(R.id.similitudValue);
//        btn_next = view.findViewById(R.id.btn_showData);
        btn_again = view.findViewById(R.id.btn_again);
        btn_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Fragment fragment = new EscaneoFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commitAllowingStateLoss();
                /*ly_cedula_data_title.setVisibility(View.VISIBLE);
                ly_user_data.setVisibility(View.VISIBLE);*/
            }
        });
/*        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Hola Siguiente",Toast.LENGTH_SHORT).show();
                ly_cedula_data_title.setVisibility(View.VISIBLE);
                ly_user_data.setVisibility(View.VISIBLE);
            }
        });*/



        //FontUtils.setearFuente(getActivity(), btn_next, Typeface.BOLD);
        FontUtils.setearFuente(getActivity(), btn_again, Typeface.BOLD);

        docImageIv = view.findViewById(R.id.documentImageIv);
        realIv = view.findViewById(R.id.realImage);

        authResults = view.findViewById(R.id.authHolder);
    }

    public void clearResults() {
        edtxtNroDoc.setText("");
        edtxtFechaNac.setText("");
        edtxtNombres.setText("");
        edtxtApellidos.setText("");
        docImageIv.setImageResource(R.drawable.id);
        realIv.setImageResource(R.drawable.id);
        radioGroupComparacion.clearCheck();
        radioGroupSexo.clearCheck();
        txt_similitud.setText("");
    }

    //show received results on the UI
    private void displayResults(DocumentReaderResults results) {
        clearResults();

        if (results != null && response != null) {
            setValues(results);

            /*if (results.textResult != null && results.textResult.fields != null) {
                for (DocumentReaderTextField textField : results.textResult.fields) {
                    String value = results.getTextFieldValueByType(textField.fieldType, textField.lcid);
                }
            }*/

            if (response != null && response.matchedFaces != null && response.matchedFaces.size() > 0) {
                for (Image img : request.images) {
                    if (img.id == EscaneoFragment.CAPTURED_FACE) {
                        Bitmap imgPerson = img.image();
                        double aspectRatio = (double) imgPerson.getWidth() / (double) imgPerson.getHeight();
                        imgPerson = Bitmap.createScaledBitmap(imgPerson, (int) (480 * aspectRatio), 480, false);
                        realIv.setImageBitmap(imgPerson);
                    }
                }
            } else authResults.setVisibility(View.INVISIBLE);

            Bitmap documentImage = results.getGraphicFieldImageByType(eGraphicFieldType.GF_DOCUMENT_IMAGE);
            if (documentImage != null) {
                double aspectRatio = (double) documentImage.getWidth() / (double) documentImage.getHeight();
                documentImage = Bitmap.createScaledBitmap(documentImage, (int) (480 * aspectRatio), 480, false);
                docImageIv.setImageBitmap(documentImage);
            }
        }
    }

    public void setValues(DocumentReaderResults resultado) {
        String txtFieldNombres = resultado.getTextFieldValueByType(eVisualFieldType.FT_GIVEN_NAMES);
        String txtFieldApellidos = resultado.getTextFieldValueByType(eVisualFieldType.FT_SURNAME);
        String txtFieldNroDoc = resultado.getTextFieldValueByType(eVisualFieldType.FT_DOCUMENT_NUMBER);
        String txtFieldSexo = resultado.getTextFieldValueByType(eVisualFieldType.FT_SEX);
        String txtFieldFechaNac = resultado.getTextFieldValueByType(eVisualFieldType.FT_DATE_OF_BIRTH);

        MatchedFacesPair pair = response.matchedFaces.get(0);
        double valor = pair.similarity * 100;
        if (valor >= 80) {
            radioGroupComparacion.check(R.id.radio_a);
        } else {
            radioGroupComparacion.check(R.id.radio_r);
            //btn_next.setEnabled(false);
        }

        txt_similitud.setText((int) (pair.similarity * 100) + "%");


        if (txtFieldNroDoc != null && !txtFieldNroDoc.isEmpty() && txtFieldNroDoc.matches("[0-9]+"))
            edtxtNroDoc.setText(txtFieldNroDoc);
        else edtxtNroDoc.setText("");

        if (txtFieldNombres != null)
            edtxtNombres.setText(txtFieldNombres);
        else edtxtNombres.setText("");

        if (txtFieldApellidos != null)
            edtxtApellidos.setText(txtFieldApellidos);
        else edtxtApellidos.setText("");


        if (txtFieldSexo != null)
            if (!txtFieldSexo.isEmpty() && txtFieldSexo.matches("Masculino"))
                radioGroupSexo.check(R.id.radio_m);
            else if (!txtFieldSexo.isEmpty() && txtFieldSexo.matches("Femenino"))
                radioGroupSexo.check(R.id.radio_f);
            else radioGroupSexo.clearCheck();

        if (txtFieldFechaNac != null && !txtFieldFechaNac.isEmpty())
            edtxtFechaNac.setText(parseoFecha(txtFieldFechaNac));
        else edtxtFechaNac.setText("");
    }

    private String parseoFecha(String fecha) {
        Date dateFecha = DateUtils.parse(fecha, DateUtils.Format.DATE_REGULA_RESULT);
        return DateUtils.format(dateFecha, DateUtils.Format.DATE_REGULA_RESULT);
    }
}