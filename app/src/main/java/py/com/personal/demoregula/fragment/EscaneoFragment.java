package py.com.personal.demoregula.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.regula.documentreader.api.DocumentReader;
import com.regula.documentreader.api.completions.IDocumentReaderCompletion;
import com.regula.documentreader.api.completions.IDocumentReaderInitCompletion;
import com.regula.documentreader.api.completions.IDocumentReaderPrepareCompletion;
import com.regula.documentreader.api.enums.DocReaderAction;
import com.regula.documentreader.api.enums.eGraphicFieldType;
import com.regula.documentreader.api.enums.eRFID_Password_Type;
import com.regula.documentreader.api.enums.eRPRM_ResultType;
import com.regula.documentreader.api.enums.eVisualFieldType;
import com.regula.documentreader.api.results.DocumentReaderResults;
import com.regula.documentreader.api.results.DocumentReaderScenario;
import com.regula.facesdk.FaceReaderService;
import com.regula.facesdk.enums.eInputFaceType;
import com.regula.facesdk.structs.Image;
import com.regula.facesdk.structs.MatchFacesRequest;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import py.com.personal.demoregula.R;
import py.com.personal.demoregula.utils.Utils;

import static android.app.Activity.RESULT_OK;
import static android.graphics.BitmapFactory.decodeStream;

public class EscaneoFragment extends Fragment {

    public int selectedPosition;
    public static final int REQUEST_BROWSE_PICTURE = 11;
    public boolean doRfid = false;
    public ImageView showScanner;
    public CheckBox doRfidCb;
    public ListView scenarioLv;
    public static final String DO_RFID = "doRfid";
    public Activity activity;
    public IDocumentReaderCompletion completion;
    public static EscaneoFragment instance;
    public static SharedPreferences sharedPreferences;
    public static final int CAPTURED_FACE = 1;
    public static final int DOCUMENT_FACE = 2;
    public static final String MY_SHARED_PREFS = "MySharedPrefs";
    public FragmentManager fragmentManager;
    public EscaneoResultadoFragment resultsFragment;
    public Dialog loadingDialog;

    public static EscaneoFragment getInstance(IDocumentReaderCompletion completion) {
        if (instance == null) instance = new EscaneoFragment();
        instance.completion = completion;
        return instance;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getInstance(completionDoc);
        activity = (Activity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_escaneo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(MY_SHARED_PREFS, Context.MODE_PRIVATE);
        showScanner = view.findViewById(R.id.foto_cedula);
        scenarioLv = view.findViewById(R.id.scenariosList);
        doRfidCb = view.findViewById(R.id.doRfidCb);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(!DocumentReader.Instance().getDocumentReaderIsReady()) {
        final Dialog initDialog = Utils.crearDialogoProgress(getActivity(), "Verificando Licencia...");
        prepareDatabase(getContext(), new IDocumentReaderPrepareCompletion() {
            @Override
            public void onPrepareProgressChanged(int i) {
                initDialog.setTitle("Descargando Base de Datos: " + i + "%");
            }

            @Override
            public void onPrepareCompleted(boolean b, String s) {
                init(getContext(), (b1, s1) -> {
                    if (initDialog.isShowing()) initDialog.dismiss();
                    if (!b1)
                        Toast.makeText(getContext(), "Inicializacion fallida:" + s1, Toast.LENGTH_LONG).show();
                });
            }
        });
        if (DocumentReader.Instance().getDocumentReaderIsReady())
            successfulInit();
    }

    public void prepareDatabase(Context context, final IDocumentReaderPrepareCompletion completion) {
        //preparing database files, it will be downloaded from network only one time and stored on user device
        DocumentReader.Instance().prepareDatabase(context, "Full", new
                IDocumentReaderPrepareCompletion() {
                    @Override
                    public void onPrepareProgressChanged(int i) {
                        completion.onPrepareProgressChanged(i);
                    }

                    @Override
                    public void onPrepareCompleted(boolean b, String s) {
                        completion.onPrepareCompleted(b, s);
                    }
                });
    }

    public void init(final Context context, final IDocumentReaderInitCompletion initCompletion) {
        try {
            InputStream licInput = getResources().openRawResource(R.raw.regula_2);
            int available = licInput.available();
            final byte[] license = new byte[available];
            //noinspection ResultOfMethodCallIgnored
            licInput.read(license);

            //Initializing the reader
            DocumentReader.Instance().initializeReader(context, license, (success, error) -> {
                DocumentReader.Instance().customization().edit()
                        .setShowResultStatusMessages(true)
                        .setShowStatusMessages(true);
                DocumentReader.Instance().functionality().setVideoCaptureMotionControl(true);

                //initialization successful
                if (success) {
                    successfulInit();
                }

                initCompletion.onInitCompleted(success, error);
            });

            licInput.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void successfulInit() {
        showScanner.setOnClickListener(view -> {
            //starting video processing
            DocumentReader.Instance().showScanner(Objects.requireNonNull(getContext()), completionDoc);
        });
        if (DocumentReader.Instance().getCanRFID()) {
            doRfid = sharedPreferences.getBoolean(DO_RFID, false);
            doRfidCb.setChecked(doRfid);
            doRfidCb.setOnCheckedChangeListener((compoundButton, checked) -> {
                doRfid = checked;
                sharedPreferences.edit().putBoolean(DO_RFID, checked).apply();
            });
        } else {
            doRfidCb.setVisibility(View.GONE);
        }

        //getting current processing scenario and loading available scenarios to ListView
        String currentScenario = DocumentReader.Instance().processParams().scenario;
        ArrayList<String> scenarios = new ArrayList<>();
        for (DocumentReaderScenario scenario : DocumentReader.Instance().availableScenarios) {
            if (scenario.name.matches("Ocr")) {
                scenarios.add(scenario.name);
            }
        }

        //setting default scenario
        if (currentScenario == null || currentScenario.isEmpty()) {
            currentScenario = scenarios.get(0);
            DocumentReader.Instance().processParams().scenario = currentScenario;
        }

        final ScenarioAdapter adapter =
                new ScenarioAdapter(activity, android.R.layout.simple_list_item_1, scenarios);
        selectedPosition = 0;
        try {
            selectedPosition = adapter.getPosition(currentScenario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        scenarioLv.setAdapter(adapter);

        scenarioLv.setSelection(selectedPosition);

        scenarioLv.setOnItemClickListener((adapterView, view, i, l) -> {
            //setting selected scenario to DocumentReader params
            DocumentReader.Instance().processParams().scenario = adapter.getItem(i);
            selectedPosition = i;
            adapter.notifyDataSetChanged();
        });
    }

    public class ScenarioAdapter extends ArrayAdapter<String> {

        public ScenarioAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            if (position == selectedPosition) view.setBackgroundColor(Color.LTGRAY);
            else view.setBackgroundColor(Color.TRANSPARENT);
            return view;
        }
    }

    private void matchFace(final DocumentReaderResults results, final Bitmap portrait) {
        loadingDialog = Utils.crearDialogoProgress(getActivity(), "Procesando datos...");
        loadingDialog.show();
        FaceReaderService.Instance().getFaceFromCamera(Objects.requireNonNull(getContext()), (action, capturedFace, s) -> {
            if (capturedFace != null) {
                final MatchFacesRequest request = new MatchFacesRequest();
                request.similarityThreshold = 0;

                Image img = new Image();
                img.id = CAPTURED_FACE;
                img.tag = ".jpg";
                img.imageType = eInputFaceType.ift_Live;
                img.setImage(capturedFace.image());
                request.images.add(img);

                Image port = new Image();
                port.id = DOCUMENT_FACE;
                port.tag = ".jpg";
                port.imageType = eInputFaceType.ift_DocumentPrinted;
                port.setImage(portrait);
                request.images.add(port);

                new Thread(() -> {
                    try {
                        FaceReaderService.Instance().matchFaces(request, (i, matchFacesResponse, s1) -> {
                            if (loadingDialog != null && loadingDialog.isShowing())
                                loadingDialog.dismiss();
                            resultsFragment = EscaneoResultadoFragment.getInstance(results, request, matchFacesResponse);
                            if (getActivity() != null && matchFacesResponse != null && results != null) {
                                fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.frame_container, resultsFragment).commitAllowingStateLoss();
                            } else
                                Toast.makeText(getContext(),"Ocurrió un error al obtener los datos de la cédula",Toast.LENGTH_SHORT).show();
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }else{
                loadingDialog.dismiss();
                Toast.makeText(getContext(),"Proceso Cancelado",Toast.LENGTH_SHORT).show();
            }

        });
    }

    // loads bitmap from uri
    private Bitmap getBitmap(Uri selectedImage, int targetWidth, int targetHeight) {
        ContentResolver resolver = Objects.requireNonNull(getContext()).getContentResolver();
        InputStream is = null;
        try {
            is = resolver.openInputStream(selectedImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, options);

        //Re-reading the input stream to move it's pointer to start
        try {
            is = resolver.openInputStream(selectedImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, targetWidth, targetHeight);
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return decodeStream(is, null, options);
    }

    // see https://developer.android.com/topic/performance/graphics/load-bitmap.html
    private int calculateInSampleSize(BitmapFactory.Options options, int bitmapWidth, int bitmapHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > bitmapHeight || width > bitmapWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > bitmapHeight && (halfWidth / inSampleSize) > bitmapWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //Image browsing intent processed successfully
            if (requestCode == REQUEST_BROWSE_PICTURE) {
                if (data != null && data.getData() != null) {
                    Uri selectedImage = data.getData();
                    Bitmap bmp = getBitmap(selectedImage, 1920, 1080);

                    AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
                    builder.setMessage("Inicializando");
                    builder.create().show();
                    //loadingDialog = showDialog("Processing image");

                    DocumentReader.Instance().recognizeImage(bmp, completionDoc);
                }
            }
        }
    }

    //DocumentReader processing callback
    private IDocumentReaderCompletion completionDoc = new IDocumentReaderCompletion() {
        @Override
        public void onCompleted(int action, DocumentReaderResults results, String error) {
            //processing is finished, all results are ready
            if (action == DocReaderAction.COMPLETE) {
                if (loadingDialog != null && loadingDialog.isShowing())
                    loadingDialog.dismiss();

                //Checking, if nfc chip reading should be performed
                if (sharedPreferences.getBoolean(DO_RFID, false) && results != null && results.chipPage != 0) {
                    //setting the chip's access key - mrz on car access number
                    String accessKey = null;
                    if ((accessKey = results.getTextFieldValueByType(eVisualFieldType.FT_MRZ_STRINGS)) != null && !accessKey.isEmpty()) {
                        accessKey = results.getTextFieldValueByType(eVisualFieldType.FT_MRZ_STRINGS)
                                .replace("^", "").replace("\n", "");
                        DocumentReader.Instance().rfidScenario().setMrz(accessKey);
                        DocumentReader.Instance().rfidScenario().setPacePasswordType(eRFID_Password_Type.PPT_MRZ);
                    } else if ((accessKey = results.getTextFieldValueByType(eVisualFieldType.FT_CARD_ACCESS_NUMBER)) != null && !accessKey.isEmpty()) {
                        DocumentReader.Instance().rfidScenario().setPassword(accessKey);
                        DocumentReader.Instance().rfidScenario().setPacePasswordType(eRFID_Password_Type.PPT_CAN);
                    }

                    //starting chip reading
                    DocumentReader.Instance().startRFIDReader(getContext(), (rfidAction, results1, error1) -> {
                        if (rfidAction == DocReaderAction.COMPLETE) {
                            final Bitmap rfidPortrait = results1.getGraphicFieldImageByType(eGraphicFieldType.GF_PORTRAIT,
                                    eRPRM_ResultType.RFID_RESULT_TYPE_RFID_IMAGE_DATA);
                            if (rfidPortrait != null) {
                                matchFace(results1, rfidPortrait);
                            } else {
                                resultsFragment = EscaneoResultadoFragment.getInstance(results1);
                                fragmentManager.beginTransaction().replace(R.id.frame_container, resultsFragment).addToBackStack("xxx").commitAllowingStateLoss();
                            }
                        }
                    });
                } else if (results != null) {
                    final Bitmap portrait = results.getGraphicFieldImageByType(eGraphicFieldType.GF_PORTRAIT);
                    if (portrait != null) {
                        matchFace(results, portrait);
                    } else {
                        resultsFragment = EscaneoResultadoFragment.getInstance(results);
                        fragmentManager.beginTransaction().replace(R.id.frame_container, resultsFragment).addToBackStack("xxx").commitAllowingStateLoss();
                    }
                } else {
                    resultsFragment = EscaneoResultadoFragment.getInstance(results);
                    fragmentManager.beginTransaction().replace(R.id.frame_container, resultsFragment).addToBackStack("xxx").commitAllowingStateLoss();
                }
            } else {
                //something happened before all results were ready
                if (action == DocReaderAction.CANCEL) {
                    Toast.makeText(getContext(), "Escaneo cancelado", Toast.LENGTH_LONG).show();
                } else if (action == DocReaderAction.ERROR) {
                    Toast.makeText(getContext(), "Error:" + error, Toast.LENGTH_LONG).show();
                }
            }
        }
    };


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}