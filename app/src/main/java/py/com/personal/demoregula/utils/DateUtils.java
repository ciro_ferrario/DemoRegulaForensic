package py.com.personal.demoregula.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Contiene metodos estaticos para transformar objetos Date y String entre
 * distintos formatos de fecha y hora.
 *
 * @author rlopez
 */
public class DateUtils {


    private static final SimpleDateFormat dateRegulaFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    /**
     * Convierte la cadena <code>date</code> cuyo formato es <code>format</code>
     * a un objeto <code>Date</code>.
     *
     * @param date   Cadenta en formato <code>format</code>
     * @param format Formato de la cadena
     * @return objeto <code>Date</code>
     */
    public static Date parse(String date, Format format) {
        if (TextUtils.isEmpty(date))
            return null;
        try {
            switch (format) {
                case DATE_REGULA_RESULT:
                    return dateRegulaFormat.parse(date);
                default:
                    return null;
            }
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Convierte el objeto <code>date</code> a una cadenta en formato
     * <code>format</code>.
     *
     * @param date   objeto <code>Date</code>
     * @param format formato del resultado.
     * @return <code>String</code> en formato <code>format</code>
     */
    public static String format(Date date, Format format) {
        if (date == null)
            return null;
        switch (format) {

            case DATE_REGULA_RESULT:
                return dateRegulaFormat.format(date);
            default:
                return null;
        }
    }

    /**
     * Formatos de fechas y horas soportados.
     *
     * @author rlopez
     */
    public enum Format {

        /**
         * Formato para mostrar la fecha del resultado de regula:
         * <code>"dd/MM/yyyy"</code>
         */
        DATE_REGULA_RESULT,
    }
}
